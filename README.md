# Standard Gradle template
Standard [Gradle](https://gradle.org) project template, including:
* Gradle 7, which supports all JDKs from 8 to 16
* Publishing & signing plugins
* Sources & JavaDoc JARs
* Integration test source-set `intTest`
* Build properties available at runtime (see `buildProperties` in [`build.gradle`](https://gitlab.com/PaulBenn/standard-gradle-template/-/blob/master/build.gradle))
* Useful cross-cutting dependencies:
  * [SLF4J API](http://www.slf4j.org) + [`logback-classic`](http://logback.qos.ch) binding
  * [Lombok](https://projectlombok.org), via [`io.freefair.lombok`](https://plugins.gradle.org/plugin/io.freefair.lombok)
  * [JUnit 5](https://junit.org/junit5) platform
* A good [`.gitignore`](https://gitlab.com/PaulBenn/standard-gradle-template/-/blob/master/.gitignore) file

### Dependency versions
* For the Lombok plugin version, see `lombokPluginVersion` in [`gradle.properties`](https://gitlab.com/PaulBenn/standard-gradle-template/-/blob/master/gradle.properties)
* For all other dependency versions, see the `ext` block in [`build.gradle`](https://gitlab.com/PaulBenn/standard-gradle-template/-/blob/master/build.gradle).

## IntelliJ set-up

### Checkstyle-IDEA plugin
1. Install the [Checkstyle-IDEA](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea) plugin.
2. Open **Settings** → **Other Settings** → **Checkstyle**.
3. Set your Checkstyle version to `8.23` or higher.
4. Add a third-party check with path:
   ```<user-directory>/.m2/repository/co/uk/paulbenn/checkstyle```
5. Add a configuration file from the third-party classpath, setting the filename to `checkstyle.xml`.
6. Save your settings. The Checkstyle plugin tab should now allow you to select the configuration you just added.
